extends ColorRect

var ball


var swing: bool = false
var swung: bool = false
var up: float = 0.0
var left: float = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	ball = get_node("/root/Node3D/Ball")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_color_rect_gui_input(event):
	if swung:
		return

	if event is InputEventMouseButton:
		if event.button_mask == 1:
			swing = true
		return

	if not event is InputEventMouseMotion:
		return

	if event.button_mask == 1:
		print(event.button_mask)
		
		left = (left + event.velocity.x) / 2.0
		up = (up + event.velocity.y) / 2.0
	elif swing == true:
		swing = false
		swung = true
		var forward = Vector3(0.0, 0.0, up * 0.016)
		var spin = Vector3(0.0, 0.0, -left * 0.08)
		ball.apply_impulse(forward)
		ball.apply_torque(spin)
		left = 0.0
		up = 0.0
		print(forward)
		print(spin)
		# do swingy stuff here


func _on_color_rect_mouse_exited():
	print("exit")
	swing = false
	left = 0.0
	up = 0.0
	print(up)
